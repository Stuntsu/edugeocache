var express = require('express');
var router = express.Router();

var ctrlLokacije = require('../controllers/lokacije');
var ctrlOstalo = require('../controllers/ostalo');

/* Lokacijske strani */
router.get('/', ctrlOstalo.angularApp);
router.get('/lokacija/:idLokacije', ctrlLokacije.podrobnostiLokacije);
router.get('/lokacija/:idLokacije/komentar/nov', ctrlLokacije.dodajKomentar);
router.post('/lokacija/:idLokacije/komentar/nov', ctrlLokacije.shraniKomentar);

/* Ostale strani */
router.get('/informacije', ctrlOstalo.informacije);

module.exports = router;
